require 'test_helper'

class PersonControllersControllerTest < ActionController::TestCase
  setup do
    @person_controller = person_controllers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:person_controllers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create person_controller" do
    assert_difference('PersonController.count') do
      post :create, person_controller: {  }
    end

    assert_redirected_to person_controller_path(assigns(:person_controller))
  end

  test "should show person_controller" do
    get :show, id: @person_controller
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @person_controller
    assert_response :success
  end

  test "should update person_controller" do
    patch :update, id: @person_controller, person_controller: {  }
    assert_redirected_to person_controller_path(assigns(:person_controller))
  end

  test "should destroy person_controller" do
    assert_difference('PersonController.count', -1) do
      delete :destroy, id: @person_controller
    end

    assert_redirected_to person_controllers_path
  end
end
