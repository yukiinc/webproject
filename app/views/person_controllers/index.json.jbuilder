json.array!(@person_controllers) do |person_controller|
  json.extract! person_controller, :id
  json.url person_controller_url(person_controller, format: :json)
end
